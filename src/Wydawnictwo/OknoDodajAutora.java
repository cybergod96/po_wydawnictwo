package Wydawnictwo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.TableView.TableRow;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class OknoDodajAutora extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtImie;
	private JTextField txtNazwisko;
	private ArrayList<Autor> autorzy;
	private boolean zaakceptowano;
	private String imieA, nazwiskoA;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			OknoDodajAutora dialog = new OknoDodajAutora();
			
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean isAlpha(String name) {
		char[] chars1 = name.toCharArray();

	    for (char c : chars1) {
	        if(!Character.isLetter(c)) {
	            return false;
	        }
	    }

	    return true;
	}
	public void pobierzListe(ArrayList<Autor>autorzy)
	{
	
		this.autorzy = autorzy;
	}

	public boolean czyZaakceptowano()
	{
		return zaakceptowano;
	}
	
	public String getImie()
	{
		return txtImie.getText();
	}
	public String getNazwisko()
	{
		return txtNazwisko.getText();
	}
	
	/**
	 * Create the dialog.
	 */
	public OknoDodajAutora() {
		setTitle("Dodaj autora");
		setBounds(100, 100, 247, 166);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblImi = new JLabel("Imi\u0119");
			lblImi.setBounds(12, 13, 56, 16);
			contentPanel.add(lblImi);
		}
		{
			JLabel lblNazwisko = new JLabel("Nazwisko");
			lblNazwisko.setBounds(12, 42, 56, 16);
			contentPanel.add(lblNazwisko);
		}
		{
			txtImie = new JTextField();
			txtImie.setBounds(80, 10, 116, 22);
			contentPanel.add(txtImie);
			txtImie.setColumns(10);
		}
		{
			txtNazwisko = new JTextField();
			txtNazwisko.setBounds(80, 39, 116, 22);
			contentPanel.add(txtNazwisko);
			txtNazwisko.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						zaakceptowano = true;	
						if(txtNazwisko.getText().isEmpty())
							JOptionPane.showMessageDialog(null, "Wype�nij nazwisko autora");
						if(txtImie.getText().isEmpty())
							JOptionPane.showMessageDialog(null, "Wype�nij imi� autora");
						if(!isAlpha(txtNazwisko.getText()))
							JOptionPane.showMessageDialog(null, "W polu nazwisko u�yj tylko liter");
						if(!isAlpha(txtImie.getText()))
							JOptionPane.showMessageDialog(null, "W polu imie u�yj tylko liter");
						if(!txtImie.getText().isEmpty()&&!(txtNazwisko.getText().isEmpty())&&isAlpha(txtImie.getText())&&isAlpha(txtNazwisko.getText()))
					dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				
				
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						zaakceptowano = false;
						dispose();
					}
				});
				
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
