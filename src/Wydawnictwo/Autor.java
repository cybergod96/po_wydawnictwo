package Wydawnictwo;

import java.util.ArrayList;
import java.util.Random;

/**
 * Klasa reprezentuj�ca Autora.
 * @author Daniel Chrzanowski
 *
 */
public class Autor implements java.io.Serializable {
	

	private static final long serialVersionUID = 1L;
	
	/**
	* Imi� danego autora i nazwisko.
	*/
	String imie, nazwisko;
	
	/**
	* Lista wszystkich teskt�w danego autora.
	* @see TekstKultury
	*/
	ArrayList<TekstKultury> teksty;

	/**
	 * Konstruktor inicjalizuj�cy obiekt.
	 * @param imie - imi� danego autora.
	 * @param nazwisko - nazwisko danego autora.
	 */
	public Autor(String imie, String nazwisko) {
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.teksty = new ArrayList<TekstKultury>();
	}

	/**
	 * Dodaje ksi��k� do listy tekst�w autora, kt�re wydawnictwo mo�e opublikowa�.
	 * @param tytul - tytu� danej ksi��ki.
	 * @param gatunek - nazwisko danego autora.
	 */
	public void napiszKsiazke(String tytul, GatunekKsiazki gatunek) {
		Random generator = new Random();
		teksty.add(new Ksiazka(tytul, generator.nextInt(200) + 100, gatunek));
	}
	
	/**
	 * Dodaje czasopismo do listy tekst�w autora, kt�re wydawnictwo mo�e opublikowa�.
	 * @param tytul - tytu� danego czasopisma.
	 * @param czyTygodnik - sprawdza czy dane czasopismo to tygodnik.
	 */
	public void napiszCzasopismo(String tytul, boolean czyTygodnik) {
		Random generator = new Random();
		if (czyTygodnik)
			teksty.add(new Tygodnik(tytul, generator.nextInt(20) + 15));
		else
			teksty.add(new Miesiecznik(tytul, generator.nextInt(20) + 20));
	}

	public String toString() {
		return imie + "\t" + nazwisko + "\t" + teksty.size();

	}

	/**
	 * Zwraca list� wszystkich tekst�w danego autora.
	 * @return Lista tekst�w.
	 * @see TekstKultury
	 */
	public ArrayList<TekstKultury> getTeksty() {
		return teksty;
	}
}
