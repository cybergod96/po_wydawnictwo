package Wydawnictwo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Typ wyliczeniowy reprezentuj�cy priorytet zam�wienia.
 * @author Piotr Cywoniuk
 *
 */
enum Priorytet
{
	Wysoki("Wysoki"), Sredni("Sredni"), Niski("Niski");
	String str;
	Priorytet(String s)
	{
		str = s;
	}
	public String toString()
	{
		return str;
	}
}

/**
 * Klasa reprezentuj�ca zam�wienie druku.
 * @author Piotr Cywoniuk
 *
 */
public class ZamowienieDruku implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * Priorytet zam�wninia
	 */
	Priorytet priorytet;
	/**
	 * Czas rozpocz�cia realizacji zam�wienia.
	 */
	Date czasRozpoczecia;
	/**
	 * Czas zako�czenia realizacji zam�wienia.
	 */
	Date czasZakonczenia;
	/**
	 * Ilo�� egzemplarzy jaka ma by� wydrukowana.
	 */
	long zleconyNaklad;
	/**
	 * Ilo�� ju� wydrukowanych egzemplarzy.
	 */
	long juzWydrukowane;
	TekstKultury tekstKultury;
	
	public Priorytet getPriorytet() {
		return priorytet;
	}

	public Date getCzasRozpoczecia() {
		return czasRozpoczecia;
	}

	public Date getCzasZakonczenia() {
		return czasZakonczenia;
	}

	public long getZleconyNaklad() {
		return zleconyNaklad;
	}

	public long getJuzWydrukowane() {
		return juzWydrukowane;
	}

	public TekstKultury getTekstKultury() {
		return tekstKultury;
	}
	
	public String toString()
	{
		SimpleDateFormat formatDaty = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return new String(formatDaty.format(czasRozpoczecia)+"\t"+(!czyWykonane()?" ":formatDaty.format(czasZakonczenia))+"\t"+zleconyNaklad+"\t"+juzWydrukowane+"\t"+priorytet.toString()+"\t"+tekstKultury.toString());
	}
	
	/**
	 * Aktualizuje stan zam�wienia.
	 * @param czas - data, kt�ra pos�u�y do aktualizacji stanu zam�wie�
	 * @param szybkoscDruku - szybko�� druku (w stronach na minut�)
	 */
	public void aktualizujStan(Date czas, double szybkoscDruku)
	{
		long roznica = (czas.getTime() - czasRozpoczecia.getTime())/60000;
		juzWydrukowane += ((roznica*szybkoscDruku)/tekstKultury.iloscStron);
		if(juzWydrukowane >= zleconyNaklad) 
		{
			juzWydrukowane = zleconyNaklad;
			czasZakonczenia = czas;
			tekstKultury.setDataWydania(czas);
		}
	}
	
	public ZamowienieDruku(TekstKultury tekstKultury, int naklad, Priorytet priorytet, Date data)
	{
		this.tekstKultury = tekstKultury;
		this.zleconyNaklad = naklad;
		this.priorytet = priorytet;
		this.czasRozpoczecia = data;
		this.czasZakonczenia = new Date(0);
		this.juzWydrukowane=0;
	}
	
	/**
	 * Sprawdza, czy wykonano zam�wienie
	 * @return zwraca true je�li wydrukowano zlecony nak�ad
	 */
	boolean czyWykonane()
	{
		return juzWydrukowane == zleconyNaklad;
	}
}
