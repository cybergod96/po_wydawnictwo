package Wydawnictwo;

import java.util.Random;

/**
 * Typ wyliczeniowy reprezentuj�cy gatunki ksi��ki.
 *
 */
enum GatunekKsiazki {

	Sensacyjna("Sensacyjna"), Romans("Romans"), Album("Album"), SciFi("SciFi"), Podrecznik("Podrecznik"), Naukowa(
			"Naukowa");
	String gatunek;

	GatunekKsiazki(String g) {
		gatunek = g;
	}

	public String toString() {
		return gatunek;
	}
}

/**
 * Klasa reprezentuj�ca Ksi��ke.
 * @author Daniel Chrzanowski
 *
 */
public class Ksiazka extends TekstKultury implements java.io.Serializable {


	private static final long serialVersionUID = 1L;
	
	/**
	* Niepowtarzalny 13-cyfrowy identyfikator ksi��ki.
	*/
	String isbn;
	
	/**
	* Dany gatunek ksi��ki.
	* @see GatunekKsiazki
	*/
	GatunekKsiazki gatunek;

	/**
	 * Konstruktor inicjalizuj�cy obiekt.
	 * @param tytul - tytu� danej ksi��ki.
	 * @param iloscStron - ilo�� stron danej ksi��ki.
	 * @param gatunek - gatunek danej ksi��ki.
	 */
	public Ksiazka(String tytul, int iloscStron, GatunekKsiazki gatunek) {
		super(tytul, iloscStron);
		this.gatunek = gatunek;

		Random generator = new Random();
		char[] chars = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 13; i++) {
			sb.append(chars[generator.nextInt(chars.length)]);
		}
		this.isbn = sb.toString();
	}

	/**
	 * Zwraca dany gatunek ksi��ki.
	 * @return Dany gatunek ksi��ki.
	 */
	public GatunekKsiazki getGatunek() {
		return gatunek;
	}

	public String toString() {
		if (dataWydania != null)
			return "Ksi��ka" + "\t" + gatunek + "\t" + isbn + "\t" + tytul + "\t" + iloscStron + "\t" + dataWydania;
		else
			return "Ksi��ka" + "\t" + gatunek + "\t" + isbn + "\t" + tytul + "\t" + iloscStron + "\t" + "Nie wydano";
	}

}