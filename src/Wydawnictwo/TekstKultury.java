package Wydawnictwo;

import java.util.Date;

/**
 * Klasa abstrakcyjna reprezentuj�ca tekst kultury.
 * @author Daniel Chrzanowski
 *
 */
public abstract class TekstKultury implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	* Tytu� danego tekstu.
	*/
	String tytul;
	
	/**
	* Ilo�� stron danego tekstu.
	*/
	int iloscStron;
	
	/**
	* Data wydania danego tekstu.
	*/
	Date dataWydania;

	/**
	 * Konstruktor inicjalizuj�cy obiekt.
	 * @param tytul - tytu� danego tekstu.
	 * @param iloscStron - ilo�� stron danego tekstu.
	 */
	public TekstKultury(String tytul, int iloscStron) {

		this.tytul = tytul;
		this.iloscStron = iloscStron;
		this.dataWydania = null;
	}

	/**
	* Sprawdza czy tekst ju� jest wydany
	* @return zwraca true, je�li jest ustawiona data wydania
	*/
	public boolean czyWydany() {
		return dataWydania != null;
	}

	public String toString() {
		return null;
	}

	/**
	* Zmiana daty wydania.
	* @param data data
	*/
	public void setDataWydania(Date data) {
		this.dataWydania = data;
	}
}
