package Wydawnictwo;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class TestUsuwaniaRealizowaniaZamowien {

	@Test
	public void test() {
		ZamowienieDruku z = new ZamowienieDruku(new Ksiazka("Testowa ksi��ka",100,GatunekKsiazki.Sensacyjna), 100, Priorytet.Wysoki, new Date(0));
		Date nowaData = new Date(1234567121);
		z.aktualizujStan(nowaData, 10);
		assertEquals(z.juzWydrukowane, z.zleconyNaklad);
	}

}
