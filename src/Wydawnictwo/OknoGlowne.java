package Wydawnictwo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.BoxLayout;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;

public class OknoGlowne {

	private JFrame frmWydawnictwo;
	private Wydawnictwo wydawnictwo;
	private Date dataSystemowa;
	private JTable tableListaUmow;
	//private JTable tableListaZamowien;
	private JTable tableZamowieniaDrukarnia1;
	private JTable tableZamowieniaDrukarnia2;
	private JTable tableZamowieniaDrukarnia3;
	private JSpinner spinnerRok = new JSpinner();
	private JSpinner spinnerMiesiac = new JSpinner();
	private JSpinner spinnerDzien = new JSpinner();
	private JSpinner spinnerGodzina = new JSpinner();
	private JSpinner spinnerMinuty = new JSpinner();
	private JLabel lblAktualnaData = new JLabel("Aktualna data:");
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OknoGlowne window = new OknoGlowne();
					window.frmWydawnictwo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OknoGlowne() {
		initialize();
	}
	
	private void aktualizujTabeleZamowien()
	{
//		DefaultTableModel model = (DefaultTableModel) tableListaZamowien.getModel();
//		for(int i = model.getRowCount()-1; i >= 0; i-- )
//		{
//			model.removeRow(i);
//		}
//		
//		for(int i = 0; i < 3; i++)
//		{
//			for(ZamowienieDruku z: wydawnictwo.getDzialDruku().przegladZamowien(i))
//			{
//				String daneZamowienia[] = z.toString().split("\t");
//				model.addRow(new Object[]{
//						daneZamowienia[0],daneZamowienia[1],daneZamowienia[2], (int)(z.getJuzWydrukowane()/z.getZleconyNaklad()*100)+"%",
//						z.getPriorytet().toString(), wydawnictwo.getDzialDruku().getDrukarnia(i).getNazwa()
//				});
//			}
//		}
//		
//		//ta z kartami	
		//drukarnia1
		DefaultTableModel modelDrukarnia1 = (DefaultTableModel) tableZamowieniaDrukarnia1.getModel();
		for(int i = modelDrukarnia1.getRowCount()-1; i >= 0; i-- )
		{
			modelDrukarnia1.removeRow(i);
		}
		for (ZamowienieDruku z : wydawnictwo.getDzialDruku().przegladZamowien(0)) 
		{
			String daneZamowienia[] = z.toString().split("\t");
			modelDrukarnia1.addRow(new Object[] { daneZamowienia[0], daneZamowienia[1], daneZamowienia[2],
					(int) ((z.getJuzWydrukowane() / (double)z.getZleconyNaklad()) * 100) + "%", z.getPriorytet().toString(),
					wydawnictwo.getDzialDruku().getDrukarnia(0).getNazwa() });
		}
		//drukarnia2
		DefaultTableModel modelDrukarnia2 = (DefaultTableModel) tableZamowieniaDrukarnia2.getModel();
		for(int i = modelDrukarnia2.getRowCount()-1; i >= 0; i-- )
		{
			modelDrukarnia2.removeRow(i);
		}
		for (ZamowienieDruku z : wydawnictwo.getDzialDruku().przegladZamowien(1)) 
		{
			String daneZamowienia[] = z.toString().split("\t");
			modelDrukarnia2.addRow(new Object[] { daneZamowienia[0], daneZamowienia[1], daneZamowienia[2],
					(int) (z.getJuzWydrukowane() / z.getZleconyNaklad() * 100) + "%", z.getPriorytet().toString(),
					wydawnictwo.getDzialDruku().getDrukarnia(1).getNazwa() });
		}
		//drukarnia1
		DefaultTableModel modelDrukarnia3 = (DefaultTableModel) tableZamowieniaDrukarnia3.getModel();
		for(int i = modelDrukarnia3.getRowCount()-1; i >= 0; i-- )
		{
			modelDrukarnia3.removeRow(i);
		}
		for (ZamowienieDruku z : wydawnictwo.getDzialDruku().przegladZamowien(2)) 
		{
			String daneZamowienia[] = z.toString().split("\t");
			modelDrukarnia3.addRow(new Object[] { daneZamowienia[0], daneZamowienia[1], daneZamowienia[2],
					(int) (z.getJuzWydrukowane() / z.getZleconyNaklad() * 100) + "%", z.getPriorytet().toString(),
					wydawnictwo.getDzialDruku().getDrukarnia(2).getNazwa() });
		}
		
		
	}
	
	private void aktualizujTabeleUmow()
	{
		DefaultTableModel model = (DefaultTableModel) tableListaUmow.getModel();
		for(int i = model.getRowCount()-1; i >= 0; i-- )
		{
			model.removeRow(i);
		}
		

		for(Umowa u: wydawnictwo.getDzialProgramowy().przegladUmow())
		{
			String daneUmowy[] = u.toString().split("\t");
			String daneAutora[] = u.getAutor().toString().split("\t");
			model.addRow(new Object[]{
					daneUmowy[0], daneAutora[0]+" "+daneAutora[1],daneUmowy[1],daneUmowy[2],daneUmowy[3],daneUmowy[4]
			});
		}
		
	}
	
	private void aktualizujCzas()
	{
		SimpleDateFormat formatDaty = new SimpleDateFormat("yyyy;MM;dd;HH;mm");
		String daneDaty[] = formatDaty.format(dataSystemowa).split(";");
		spinnerRok.setValue(Integer.parseInt(daneDaty[0]));
		spinnerMiesiac.setValue(Integer.parseInt(daneDaty[1]));
		spinnerDzien.setValue(Integer.parseInt(daneDaty[2]));
		spinnerGodzina.setValue(Integer.parseInt(daneDaty[3]));
		spinnerMinuty.setValue(Integer.parseInt(daneDaty[4]));
		formatDaty.applyPattern("yyyy-MM-dd HH:mm");
		lblAktualnaData.setText("Aktualna data: "+formatDaty.format(dataSystemowa));
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		wydawnictwo = new Wydawnictwo();
		dataSystemowa = new Date();
		frmWydawnictwo = new JFrame();
		frmWydawnictwo.setTitle("Wydawnictwo");
		frmWydawnictwo.setBounds(100, 100, 908, 637);
		frmWydawnictwo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmWydawnictwo.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panelDzialProgramowy = new JPanel();
		tabbedPane.addTab("Dzia� programowy", null, panelDzialProgramowy, null);
		panelDzialProgramowy.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 13, 608, 260);
		panelDzialProgramowy.add(scrollPane);
		
		tableListaUmow = new JTable();
		tableListaUmow.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Typ umowy", "Autor", "Data rozpocz�cia", "Data zako�czenia", "Opis", "Status"
			}
		));
		scrollPane.setViewportView(tableListaUmow);
		
		JButton btnDodajUmowe = new JButton("Dodaj umow�");
	
		btnDodajUmowe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				OknoNowaUmowa okno = new OknoNowaUmowa();
				okno.wypelnijAutorow(wydawnictwo.getDzialProgramowy().getAutorzy());
				okno.setModal(true);
				okno.setVisible(true);
				 Date date = new Date();
				if(okno.czyZaakceptowano())
				{
					String daneAutora[] = okno.getDaneAutora().split("\t");
					wydawnictwo.getDzialProgramowy().zawrzyjUmowe(daneAutora[0], daneAutora[1], okno.getCzasRozpoczecia(), okno.getCzasZakonczenia(), okno.getCzyODzielo(), okno.getOpis());
					wydawnictwo.getDzialProgramowy().aktualizujStan(date);
					aktualizujTabeleUmow();
				}
			}
		});
		btnDodajUmowe.setBounds(632, 30, 117, 25);
		panelDzialProgramowy.add(btnDodajUmowe);
		
		JButton btnUsunUmowe = new JButton("Usu\u0144 umow\u0119");
		btnUsunUmowe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(tableListaUmow.getSelectedRow() != -1)
				{
					String daneAutora[] = wydawnictwo.getDzialProgramowy().przegladUmow().get(tableListaUmow.getSelectedRow()).getAutor().toString().split("\t");
					wydawnictwo.getDzialProgramowy().rozwiazUmowe(tableListaUmow.getSelectedRow());
					//wydawnictwo.getDzialProgramowy().rozwiazUmowe(daneAutora[0], daneAutora[1]);
					aktualizujTabeleUmow();
				}
				else
				{
					JOptionPane.showInternalMessageDialog(null, "Zaznacz umow�");
				}
			}
		});
		btnUsunUmowe.setBounds(632, 69, 117, 25);
		panelDzialProgramowy.add(btnUsunUmowe);
		
		JButton btnZarzadzajAutorami = new JButton("Zarz\u0105dzaj autorami");
		btnZarzadzajAutorami.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				OknoZarzadzajAutorami okno = new OknoZarzadzajAutorami();
				//okno.get
				okno.wypelnijAutorow(wydawnictwo.dzialProgramowy.getAutorzy());
				okno.setModal(true);
				okno.setVisible(true);
			}
		});
		btnZarzadzajAutorami.setBounds(632, 145, 149, 25);
		panelDzialProgramowy.add(btnZarzadzajAutorami);
		
		JButton btnUsunWygasle = new JButton("Usu\u0144 wygas\u0142e");
		btnUsunWygasle.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				wydawnictwo.getDzialProgramowy().usunWygasleUmowy();
				aktualizujTabeleUmow();
			}
		});
		btnUsunWygasle.setBounds(632, 107, 117, 25);
		panelDzialProgramowy.add(btnUsunWygasle);
		
		
		JPanel panelDzialDruku = new JPanel();
		tabbedPane.addTab("Dzia� druku", null, panelDzialDruku, null);
		panelDzialDruku.setLayout(null);
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setBounds(12, 13, 671, 260);
		panelDzialDruku.add(tabbedPane_1);
		
		JPanel panelDrukarnia1 = new JPanel();
		panelDrukarnia1.setToolTipText("");
		tabbedPane_1.addTab("["+wydawnictwo.getDzialDruku().getDrukarnia(0).toString().split("\t")[0]+"] "+wydawnictwo.getDzialDruku().getDrukarnia(0).toString().split("\t")[1], null, panelDrukarnia1, null);
		panelDrukarnia1.setLayout(new BoxLayout(panelDrukarnia1, BoxLayout.X_AXIS));
		
		JScrollPane scrollPane_2 = new JScrollPane();
		panelDrukarnia1.add(scrollPane_2);
		
		tableZamowieniaDrukarnia1 = new JTable();
		tableZamowieniaDrukarnia1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableZamowieniaDrukarnia1.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"Data rozpocz�cia", "Data zako�czenia", "Zlecony nak�ad", "Post�p", "Priorytet", "Drukarnia"
				}
				));
		scrollPane_2.setViewportView(tableZamowieniaDrukarnia1);
		
		JPanel panelDrukarnia2 = new JPanel();
		tabbedPane_1.addTab("["+wydawnictwo.getDzialDruku().getDrukarnia(1).toString().split("\t")[0]+"] "+wydawnictwo.getDzialDruku().getDrukarnia(1).toString().split("\t")[1], null, panelDrukarnia2, null);
		panelDrukarnia2.setLayout(new BoxLayout(panelDrukarnia2, BoxLayout.X_AXIS));
		
		JScrollPane scrollPane_3 = new JScrollPane();
		panelDrukarnia2.add(scrollPane_3);
		
		tableZamowieniaDrukarnia2 = new JTable();
		tableZamowieniaDrukarnia2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableZamowieniaDrukarnia2.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"Data rozpocz�cia", "Data zako�czenia", "Zlecony nak�ad", "Post�p", "Priorytet", "Drukarnia"
				}
				));
		scrollPane_3.setViewportView(tableZamowieniaDrukarnia2);
		
		JPanel panelDrukarnia3 = new JPanel();
		tabbedPane_1.addTab("["+wydawnictwo.getDzialDruku().getDrukarnia(2).toString().split("\t")[0]+"] "+wydawnictwo.getDzialDruku().getDrukarnia(2).toString().split("\t")[1], null, panelDrukarnia3, null);
		panelDrukarnia3.setLayout(new BoxLayout(panelDrukarnia3, BoxLayout.X_AXIS));
		
		JScrollPane scrollPane_4 = new JScrollPane();
		panelDrukarnia3.add(scrollPane_4);
		
		tableZamowieniaDrukarnia3 = new JTable();
		tableZamowieniaDrukarnia3.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableZamowieniaDrukarnia3.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"Data rozpocz�cia", "Data zako�czenia", "Zlecony nak�ad", "Post�p", "Priorytet", "Drukarnia"
				}
				));
		scrollPane_4.setViewportView(tableZamowieniaDrukarnia3);
		
		JButton btnNoweZamowienie = new JButton("Nowe zam\u00F3wienie");
		btnNoweZamowienie.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				OknoNoweZamowienie okno = new OknoNoweZamowienie();
				ArrayList<Autor> autorzyZUmowa = new ArrayList<Autor>();
				for(Umowa u:wydawnictwo.getDzialProgramowy().przegladUmow())
				{
					if(!u.czyWygasla())
						autorzyZUmowa.add(u.getAutor());
				}
				okno.wypelnijAutorow(autorzyZUmowa);
				ArrayList<Drukarnia> dl = new ArrayList<>();
				dl.add(wydawnictwo.getDzialDruku().getDrukarnia(0));
				dl.add(wydawnictwo.getDzialDruku().getDrukarnia(1));
				dl.add(wydawnictwo.getDzialDruku().getDrukarnia(2));
				okno.wypelnijDrukarnie(dl);
				okno.setModal(true);
				okno.setVisible(true);
				if(okno.czyZaakceptowano())
				{
					wydawnictwo.getDzialDruku().zlozZamowienie(wydawnictwo.getDzialProgramowy().getAutorzy().get(okno.getIdAutora()).getTeksty().get(okno.getIdTekstu()), okno.getNaklad(), okno.getPriorytet(), dataSystemowa, okno.getIdDrukarni());
					aktualizujTabeleZamowien();

				}
			}
		});
		btnNoweZamowienie.setBounds(695, 75, 159, 25);
		panelDzialDruku.add(btnNoweZamowienie);
		
		JButton btnUsunZamowienie = new JButton("Usu\u0144 zam\u00F3wienie");
		btnUsunZamowienie.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int indeks;
				switch(tabbedPane_1.getSelectedIndex())
				{
				case 0: 
					indeks = tableZamowieniaDrukarnia1.getSelectedRow();
					if(indeks >= 0)
					{
						wydawnictwo.getDzialDruku().getDrukarnia(0).usunZamowienie(indeks);
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Zaznacz zam�wienie.");
					}
					break;
				case 1: 
					indeks = tableZamowieniaDrukarnia2.getSelectedRow();
					if(indeks >= 0)
					{
						wydawnictwo.getDzialDruku().getDrukarnia(1).usunZamowienie(indeks);
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Zaznacz zam�wienie.");
					}
					break;
				case 2: 
					indeks = tableZamowieniaDrukarnia3.getSelectedRow();
					if(indeks >= 0)
					{
						wydawnictwo.getDzialDruku().getDrukarnia(2).usunZamowienie(indeks);
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Zaznacz zam�wienie.");
					}
					break;					
				}
				
				aktualizujTabeleZamowien();
			}
		});
		btnUsunZamowienie.setBounds(695, 113, 159, 25);
		panelDzialDruku.add(btnUsunZamowienie);
		
		JButton btnSzczegolyZamowienia = new JButton("Szczeg\u00F3\u0142y");
		btnSzczegolyZamowienia.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int idDrukarni = tabbedPane_1.getSelectedIndex(), idZamowienia=-1;
				switch(idDrukarni)
				{
				case 0: idZamowienia = tableZamowieniaDrukarnia1.getSelectedRow();break;
				case 1: idZamowienia = tableZamowieniaDrukarnia2.getSelectedRow();break;
				case 2: idZamowienia = tableZamowieniaDrukarnia3.getSelectedRow();break;
				}
				if(idZamowienia >= 0)
				{
					String daneTekstu[] = wydawnictwo.getDzialDruku().getDrukarnia(idDrukarni).getZamowienia().get(idZamowienia).getTekstKultury().toString().split("\t");
					JOptionPane.showMessageDialog(null, "Typ: "+daneTekstu[0]+
							"\nGatunek: "+daneTekstu[1]+"\nIdentyfikator: "+daneTekstu[2]+
							"\nTytu�: "+daneTekstu[3]+"\nIlo�� stron: "+daneTekstu[4]);
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Zaznacz zam�wienie.");
				}
			}
		});
		btnSzczegolyZamowienia.setBounds(695, 151, 159, 25);
		panelDzialDruku.add(btnSzczegolyZamowienia);
		
		JButton btnUsunZakonczone = new JButton("Usu\u0144 zako\u0144czone");
		btnUsunZakonczone.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				wydawnictwo.getDzialDruku().getDrukarnia(tabbedPane_1.getSelectedIndex()).usunZakonczone();
				aktualizujTabeleZamowien();
			}
		});
		btnUsunZakonczone.setBounds(695, 189, 159, 25);
		panelDzialDruku.add(btnUsunZakonczone);
		
		//Elementy panelu System
		
		spinnerRok.setModel(new SpinnerNumberModel(new Integer(2017), new Integer(1970), null, new Integer(1)));
		spinnerRok.setBounds(12, 57, 70, 22);
		
		
		spinnerMiesiac.setModel(new SpinnerNumberModel(1, 1, 12, 1));
		spinnerMiesiac.setBounds(94, 57, 70, 22);
		
		spinnerDzien.setModel(new SpinnerNumberModel(1, 1, 30, 1));
		spinnerDzien.setBounds(176, 57, 70, 22);
		
		
		spinnerGodzina.setModel(new SpinnerNumberModel(0, null, 24, 1));
		spinnerGodzina.setBounds(258, 57, 70, 22);
		
		
		spinnerMinuty.setModel(new SpinnerNumberModel(0, null, 60, 1));
		spinnerMinuty.setBounds(340, 57, 70, 22);
		
		
		lblAktualnaData.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAktualnaData.setBounds(12, 13, 556, 31);
		
		//Panel system
		JPanel panelSystem = new JPanel();
		
				tabbedPane.addTab("System", null, panelSystem, null);
				panelSystem.setLayout(null);
				panelSystem.add(spinnerRok);
				panelSystem.add(spinnerMiesiac);
				panelSystem.add(spinnerDzien);
				panelSystem.add(spinnerGodzina);	
				panelSystem.add(spinnerMinuty);
				panelSystem.add(lblAktualnaData);
				
				JButton btnUstawDate = new JButton("Ustaw dat\u0119");
				btnUstawDate.addMouseListener(new MouseAdapter() {
					@Override
					/**
					 * Fukncja ustawia dat� w programie i nakazuje aktualizacj� stanu wydawnictwa
					 */
					public void mouseClicked(MouseEvent arg0) {
						
						SimpleDateFormat formatDaty = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						String daneDaty = spinnerRok.getValue()+"-"+String.format("%02d", spinnerMiesiac.getValue())+"-"+String.format("%02d", spinnerDzien.getValue())+" "+String.format("%02d", spinnerGodzina.getValue())+":"+String.format("%02d", spinnerMinuty.getValue());
						//System.out.println(daneDaty);
						try {
							dataSystemowa = formatDaty.parse(daneDaty);
							SimpleDateFormat formatDaty2 = new SimpleDateFormat("yyyy;MM;dd;HH;mm");
							String daneDaty2[] = formatDaty2.format(dataSystemowa).split(";");
							spinnerRok.setValue(Integer.parseInt(daneDaty2[0]));
							spinnerMiesiac.setValue(Integer.parseInt(daneDaty2[1])); 
							spinnerDzien.setValue(Integer.parseInt(daneDaty2[2]));
							spinnerGodzina.setValue(Integer.parseInt(daneDaty2[3]));
							spinnerMinuty.setValue(Integer.parseInt(daneDaty2[4]));
						} catch (ParseException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
						
						lblAktualnaData.setText("Aktualna data: "+formatDaty.format(dataSystemowa));
						wydawnictwo.aktualiujStan(dataSystemowa);
						aktualizujTabeleUmow();
						aktualizujTabeleZamowien();
						
					}
				});
				btnUstawDate.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					}
				});
				btnUstawDate.setBounds(104, 92, 178, 25);
				panelSystem.add(btnUstawDate);
				
				JButton btnZapiszStan = new JButton("Zapisz stan");
				btnZapiszStan.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						JFileChooser nazwaPliku = new JFileChooser();
						FileNameExtensionFilter filtr = new FileNameExtensionFilter("Stan systemu", "dat");
						nazwaPliku.setFileFilter(filtr);
						if(nazwaPliku.showSaveDialog(null) != JFileChooser.APPROVE_OPTION)return;
						try {
							FileOutputStream plik = new FileOutputStream(nazwaPliku.getSelectedFile().getAbsolutePath());
							ObjectOutputStream objOut = new ObjectOutputStream(plik);
							objOut.writeObject(dataSystemowa);
							//wydawnictwo.zapiszStan(objOut);
							objOut.writeObject(wydawnictwo);
							objOut.close();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(null,"Zapisywanie nieudane:\n"+ e1.getMessage());
						}
					}
				});
				btnZapiszStan.setBounds(6, 485, 103, 25);
				panelSystem.add(btnZapiszStan);
				
				JButton btnWczytajStan = new JButton("Wczytaj stan");
				btnWczytajStan.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						JFileChooser nazwaPliku = new JFileChooser();
						FileNameExtensionFilter filtr = new FileNameExtensionFilter("Stan systemu", "dat");
						nazwaPliku.setFileFilter(filtr);
						if(nazwaPliku.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)return;
						try {
							FileInputStream plik = new FileInputStream(nazwaPliku.getSelectedFile().getAbsolutePath());
							ObjectInputStream objIn = new ObjectInputStream(plik);
							dataSystemowa = (Date)objIn.readObject();
							wydawnictwo = (Wydawnictwo)objIn.readObject();
							objIn.close();
							aktualizujTabeleUmow();
							aktualizujTabeleZamowien();
							aktualizujCzas();
						} catch (ClassNotFoundException | IOException e1) {
							JOptionPane.showMessageDialog(null, "Wczytywanie nieudane:\n"+e1.getMessage());
						}
					}
				});
				btnWczytajStan.setBounds(114, 485, 111, 25);
				panelSystem.add(btnWczytajStan);
		
		//aktualizowanie etykiety z dat� po przej�ciu do panelu System
		tabbedPane.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				if(tabbedPane.getSelectedIndex() == 2)
				{
//					SimpleDateFormat formatDaty = new SimpleDateFormat("yyyy;MM;dd;HH;mm");
//					String daneDaty[] = formatDaty.format(dataSystemowa).split(";");
//					spinnerLata.setValue(Integer.parseInt(daneDaty[0]));
//					spinnerMiesiace.setValue(Integer.parseInt(daneDaty[1]));
//					spinnerDni.setValue(Integer.parseInt(daneDaty[2]));
//					spinnerGodziny.setValue(Integer.parseInt(daneDaty[3]));
//					spinnerMinuty.setValue(Integer.parseInt(daneDaty[4]));
//					formatDaty.applyPattern("yyyy-MM-dd HH:mm");
//					lblAktualnaData.setText("Aktualna data: "+formatDaty.format(dataSystemowa));
					aktualizujCzas();
				}
				if(tabbedPane.getSelectedIndex() == 1)
				{
					aktualizujTabeleZamowien();
				}
				if(tabbedPane.getSelectedIndex() == 0)
				{
					aktualizujTabeleUmow();
				}
				
			}
		});
		aktualizujTabeleUmow();
		aktualizujTabeleZamowien();
	}
}
