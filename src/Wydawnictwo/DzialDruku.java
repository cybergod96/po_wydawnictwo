package Wydawnictwo;

import java.util.ArrayList;
import java.util.Date;

/**
 * Klasa reprezentuj�ca dzia� druku wydawnictwa.
 * @author Piotr Cywoniuk
 */
public class DzialDruku implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;
	/**
	 * Tablica drukarni, kt�re posiada wydawnictwo.
	 */
	Drukarnia[] drukarnie;
	
	/**
	 * Konstruktor inicjalizuj�cy drukarnie.
	 */
	public DzialDruku()
	{
		drukarnie = new Drukarnia[3];
		drukarnie[0] = new DrukarniaZwyklych("Drukarnia 1", 10, 20);
		drukarnie[1] = new DrukarniaZwyklych("Drukarnia 2", 10, 20);
		drukarnie[2] = new DrukarniaAlbumow("Drukarnia 3",6,10);
	}
	
	/**
	 * Metoda realizuj�ca z�o�enie zam�wienia. Rodziela odpowiednio zam�wienia.
	 * @param tekstKultury tekst kultury, jaki ma zosta� wydrukowany
	 * @param naklad ilo�� egzemplarzy do wydrukowania
	 * @param priorytet priorytet zam�wienia
	 * @param data data przyj�cia zam�wienia
	 * @param drukarnia numer drukarni
	 */
	public void zlozZamowienie(TekstKultury tekstKultury, int naklad, Priorytet priorytet, Date data, int drukarnia)
	{
		drukarnie[drukarnia].przyjmijZamowienie(tekstKultury, naklad, priorytet, data);
	}
	
	/**
	 * Zwraca list� zam�wie� dla konkretnej drukarni.
	 * @param indeksDrukarni numer drukarni
	 * @return Lista zam�wie� konkretnej drukarni.
	 */
	public ArrayList<ZamowienieDruku> przegladZamowien(int indeksDrukarni)
	{
		return drukarnie[indeksDrukarni].getZamowienia();
	}
	
	/**
	 * Nakazuje drukarniom aktualizacj� stanu zam�wie�
	 * @param czas - data, kt�ra pos�u�y do aktualizacji stanu zam�wie�
	 */
	public void aktualizujStan(Date czas)
	{
		for(int i = 0; i < drukarnie.length; i++)
		{
			drukarnie[i].aktualizujStan(czas);
		}
	}
	
	/**
	 * Pobiera obiekt drukarni.
	 * @param index numer drukarni
	 * @return obiekt drukarni o podanym numerze
	 */
	public Drukarnia getDrukarnia(int index)
	{
		return drukarnie[index];
	}
}
