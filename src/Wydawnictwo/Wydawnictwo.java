package Wydawnictwo;

import java.util.Date;

/**
 * G��wna klasa reprezentuj�ca wydawnictwo. 
 * @author Piotr Cywoniuk
 *
 */
public class Wydawnictwo implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Obiekt reprezentuj�cy dzia� druku.
	 * 
	 * @see DzialDruku
	 */
	DzialDruku dzialDruku;
	/**
	 * Obiekt reprezentuj�cy dzia� programowy.
	 * 
	 * @see DzialProgramowy
	 */
	DzialProgramowy dzialProgramowy;
	
	/**
	 * 
	 * @return Wska�nik do obiektu klasy DzialDruku
	 * @see DzialDruku
	 */
	DzialDruku getDzialDruku()
	{
		return dzialDruku;
	}
	
	/**
	 * 
	 * @return Zwraca wska�nik do obiektu klasy DzialProgramowy
	 * @see DzialProgramowy
	 */
	DzialProgramowy getDzialProgramowy()
	{
		return dzialProgramowy;
	}
	
	/**
	 * Inicjalizuje obiekty klas DzialDruku i DzialProgramowy
	 */
	public Wydawnictwo()
	{
		dzialDruku = new DzialDruku();
		dzialProgramowy = new DzialProgramowy();
	}
	
	/**
	 * Nakazuje dzia�om aktualizacj� swojego stanu.
	 * @param czas - data, kt�ra pos�u�y do aktualizacji stanu
	 */
	public void aktualiujStan(Date czas)
	{
		dzialDruku.aktualizujStan(czas);
		dzialProgramowy.aktualizujStan(czas);
	}
}
