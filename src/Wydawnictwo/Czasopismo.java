package Wydawnictwo;

import java.util.Random;

/**
 * Klasa abstrakcyjna reprezentująca czasopismo.
 * @author Daniel Chrzanowski
 *
 */
abstract class Czasopismo extends TekstKultury implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	* ośmiocyfrowy niepowtarzalny identyfikator wydawnictw ciągłych
	*/
	String issn;

	/**
	 * Konstruktor inicjalizujący obiekt.
	 * @param tytul - tytuł danego czasopisma.
	 * @param iloscStron - ilość stron danego czasopisma.
	 */
	public Czasopismo(String tytul, int iloscStron) {
		super(tytul, iloscStron);
		Random generator = new Random();
		char[] chars = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 8; i++) {
			sb.append(chars[generator.nextInt(chars.length)]);
		}

		this.issn = sb.toString();
	}

	public String toString() {
		return null;

	}
}