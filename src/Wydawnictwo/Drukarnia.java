package Wydawnictwo;
import Wydawnictwo.ZamowienieDruku;
import java.util.ArrayList;
import java.util.Date;

/**
 * Klasa abstrakcyjna reprezentuj�ca drukarni�.
 * @author Piotr Cywoniuk
 *
 */
public abstract class Drukarnia implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * Nazwa drukarni.
	 */
	String nazwa;
	/**
	 * Szybko��, z jak� dukuje drukarnia w stronach na minut�.
	 */
	double szybkoscDrukuStrony;
	/**
	 * Ilo�� drukarek, jak� posiada drukarnia.
	 */
	int iloscDrukarek;
	/**
	 * Lista zam�wie� zleconych drukarni.
	 * @see ZamowienieDruku
	 */
	ArrayList<ZamowienieDruku> zamowienia;
	
	/**
	 * Konstruktor inicjalizuj�cy obiekt.
	 * @param nazwa - nazwa drukarni
	 * @param szybkoscDrukuStrony - szybko��, z jak� drukuj� drukarki w drukarni (w stronach na minut�)
	 * @param iloscDrukarek - ilo�� drukarek jak� posiada drukarnia
	 */
	public Drukarnia(String nazwa, double szybkoscDrukuStrony, int iloscDrukarek)
	{
		this.nazwa = nazwa;
		this.szybkoscDrukuStrony = szybkoscDrukuStrony;
		this.iloscDrukarek = iloscDrukarek;
		zamowienia = new ArrayList<ZamowienieDruku>();
	}
	
	/**
	 * Zwraca list� zam�wie�
	 * @return Lista wszystkich zam�wie�.
	 */
	public ArrayList<ZamowienieDruku> getZamowienia()
	{
		return zamowienia;
	}
	
	/**
	 * Nakazuje zam�wieniom aktualizacj� stanu. Je�li wydrukowano zlecon� ilo�� stron i egzemplarzy, dla tekstu kultury ustawiana jest data wdania.
	 * @param czas - data, kt�ra pos�u�y do aktualizacji stanu zam�wie�
	 */
	public void aktualizujStan(Date czas)
	{
		int licznik = 0;
		for(int i = 0; i < zamowienia.size(); i++)
		{
			//zamowienia.get(i).aktualizujStan(czas, szybkoscDrukuStrony);
			if(!zamowienia.get(i).czyWykonane() && licznik < iloscDrukarek)
			{
				zamowienia.get(i).aktualizujStan(czas, szybkoscDrukuStrony);
				licznik++;
			}
		}		
	}

	/**
	 * Usuwa wykonane zlecenia.
	 */
	public void usunZakonczone()
	{
		for(int i = zamowienia.size()-1; i >= 0; i--)
		{
			ZamowienieDruku z = zamowienia.get(i);
			if(z.czyWykonane())
			{
				zamowienia.remove(i);
			}
		}
	}
	
	/**
	 * Dodaje nowe zam�wienie umieszczaj�c je na li�cie adekwatnie do priorytetu i daty z�o�enia.
	 * @param tekstKultury - tekst kultury, jaki ma zosta� wydrukowany
	 * @param naklad - ilo�� egzemplarzy do wydrukowania
	 * @param priorytet - priorytet zam�wienia
	 * @param data - data przyj�cia zam�wienia
	 */
	public void przyjmijZamowienie(TekstKultury tekstKultury, int naklad, Priorytet priorytet, Date data)
	{
		if(zamowienia.isEmpty())
		{
			zamowienia.add(new ZamowienieDruku(tekstKultury, naklad, priorytet, data));
			return;
		}
		for(int i = 0; i < zamowienia.size(); i++)
		{
			switch(priorytet)
			{
			case Niski:
				zamowienia.add(new ZamowienieDruku(tekstKultury, naklad, priorytet, data));
				return;
			case Sredni:
				switch(zamowienia.get(i).getPriorytet())
				{
				case Wysoki:
					if(i+1 < zamowienia.size())
					{
						if(zamowienia.get(i+1).getPriorytet() == Priorytet.Niski)
						{
							zamowienia.add(i+1,new ZamowienieDruku(tekstKultury, naklad, priorytet, data));
							return;
						}
					}
					else
					{
						zamowienia.add(new ZamowienieDruku(tekstKultury, naklad, priorytet, data));
						return;
					}
					break;
				case Sredni:
					if(i+1 < zamowienia.size() )
					{
						if(zamowienia.get(i+1).getPriorytet() == Priorytet.Niski)
						{
							zamowienia.add(i+1, new ZamowienieDruku(tekstKultury, naklad, priorytet, data));
							return;
						}
					}
					else
					{
						zamowienia.add(new ZamowienieDruku(tekstKultury, naklad, priorytet, data));
						return;
					}
					break;
				case Niski:
					zamowienia.add(i, new ZamowienieDruku(tekstKultury, naklad, priorytet, data));
					return;
				}
				break;
				
			case Wysoki:
				if(zamowienia.get(i).getPriorytet() == Priorytet.Wysoki)
				{
					if(i+1 >= zamowienia.size())
						{
							zamowienia.add(new ZamowienieDruku(tekstKultury, naklad, priorytet, data));
							return;
						}
					else if(zamowienia.get(i+1).getPriorytet() != Priorytet.Wysoki)
						{
							zamowienia.add(i+1,new ZamowienieDruku(tekstKultury, naklad, priorytet, data));
							return;
						}
				}
				else
					{
						zamowienia.add(i,new ZamowienieDruku(tekstKultury, naklad, priorytet, data));
						return;
					}
				break;
			
			}
		}
	}
	
	public String getNazwa()
	{
		return nazwa;
	}
	
	/**
	 * @return Napis reprezentuj�cy drukarni� (poszczeg�lne warto�ci oddzielone znakiem tabulatora.
	 */
	public String toString()
	{
		return null;//new String(nazwa+"\t"+szybkoscDrukuStrony+"\t"+iloscDrukarek+"\t"+zamowienia.size());
	}
	
	public void usunZamowienie(int indeks)
	{
		zamowienia.remove(indeks);
	}
	
}
