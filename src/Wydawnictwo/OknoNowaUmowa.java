package Wydawnictwo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class OknoNowaUmowa extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ArrayList<Autor> autorzy;
	private final JComboBox cbAutorzy = new JComboBox();
	private final JRadioButton rbOPrace = new JRadioButton("O prac\u0119");
	private final JRadioButton rbOdzielo = new JRadioButton("O dzie\u0142o");
	private static final SimpleDateFormat dd= new SimpleDateFormat("yyyy-MM-dd");
	private final JFormattedTextField txtDataRozpoczecia = new JFormattedTextField(dd);
	private final JFormattedTextField txtDataZakonczenia = new JFormattedTextField(dd);
	private final JFormattedTextField dd1 = new JFormattedTextField(dd);
	private JTextField txtOpis;
	private boolean zaakceptowano;
	
	
	
	private static final Date date = new Date();
	
	//System.out.println(dateFormat.format(date));

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			OknoNowaUmowa dialog = new OknoNowaUmowa();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void wypelnijAutorow(ArrayList<Autor>autorzy)
	{
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		model.addElement("<Wybierz>");
		this.autorzy = autorzy;
		for(Autor a:autorzy)
		{
			String daneAutora[] = a.toString().split("\t");
			model.addElement(daneAutora[0]+" "+daneAutora[1]);
		}
		cbAutorzy.setModel(model);
	}
	public String getDaneAutora()
	{
		return autorzy.get(cbAutorzy.getSelectedIndex()-1).toString();
	}
	public String getOpis()
	{
		return txtOpis.getText();
	}
	public Date getCzasRozpoczecia()
	{
		return date;
	}
	public Date getCzasZakonczenia()
	{
		return (Date) txtDataZakonczenia.getValue();
	}
	public boolean czyZaakceptowano()
	{
		return zaakceptowano;
	}
	public boolean getCzyODzielo()
	{if(rbOPrace.isSelected())
		return false;
		else
		return true;
	}

	/**
	 * Create the dialog.
	 */
	public OknoNowaUmowa() {
		setTitle("Nowa umowa");
		setBounds(100, 100, 286, 251);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel label = new JLabel("Autor");
		label.setBounds(12, 16, 56, 16);
		contentPanel.add(label);
		
		//JComboBox cbAutor = new JComboBox();
		cbAutorzy.setBounds(67, 13, 169, 22);
		contentPanel.add(cbAutorzy);
		
		JLabel label_1 = new JLabel("Typ umowy");
		label_1.setBounds(12, 46, 73, 16);
		contentPanel.add(label_1);
		

		rbOPrace.setSelected(true);
		rbOPrace.setBounds(93, 42, 83, 25);
		contentPanel.add(rbOPrace);
		
		
		rbOdzielo.setBounds(180, 42, 83, 25);
		contentPanel.add(rbOdzielo);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rbOdzielo);
		group.add(rbOPrace);
		
		JLabel label_2 = new JLabel("Opis");
		label_2.setBounds(12, 75, 56, 16);
		contentPanel.add(label_2);
		
		txtOpis = new JTextField();
		txtOpis.setColumns(10);
		txtOpis.setBounds(54, 72, 182, 22);
		contentPanel.add(txtOpis);
		
		JLabel label_3 = new JLabel("Data rozpocz\u0119cia");
		label_3.setBounds(12, 104, 116, 16);
		contentPanel.add(label_3);
		
		JLabel label_4 = new JLabel("Data zako\u0144czenia");
		label_4.setBounds(12, 133, 109, 16);
		contentPanel.add(label_4);
		
		
		txtDataRozpoczecia.setBounds(127, 101, 109, 22);
		txtDataRozpoczecia.setValue(date);
		contentPanel.add(txtDataRozpoczecia);
		
		txtDataZakonczenia.setBounds(127, 130, 109, 22);
		contentPanel.add(txtDataZakonczenia);
		
		JLabel label_5 = new JLabel("");
		label_5.setBounds(49, 160, 116, 16);
		contentPanel.add(label_5);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						zaakceptowano=true;
						if(cbAutorzy.getSelectedIndex()!=0&&txtDataZakonczenia.getValue()!=null&&txtDataRozpoczecia.getValue()!=null)
							if(getCzyODzielo())
								if(txtOpis.getText().isEmpty())
									JOptionPane.showMessageDialog(null, "Wype�nij Opis");
								else
									dispose();
								else
							dispose();
						if(cbAutorzy.getSelectedIndex()==0)						
							JOptionPane.showMessageDialog(null, "Wybierz Autora");						
						
						if(txtDataZakonczenia.getValue()==null)
							JOptionPane.showMessageDialog(null, "Wype�nij Date Zakonczenia");

							
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						zaakceptowano=false;
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
