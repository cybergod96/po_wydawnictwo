package Wydawnictwo;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class TestUsuwaniaUmowy {

	@Test
	public void test() {
		DzialProgramowy d = new DzialProgramowy();
		d.zawrzyjUmowe("Zbigniew", "Raciczka", new Date(), new Date(), false, "");
		assertEquals(1,d.przegladUmow().size());
		
	}

}
