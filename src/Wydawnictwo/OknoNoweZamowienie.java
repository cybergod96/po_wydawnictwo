package Wydawnictwo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class OknoNoweZamowienie extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private final JComboBox cbAutorzy = new JComboBox();
	private final JComboBox cbTeksty = new JComboBox();
	private final JSpinner spinnerNaklad = new JSpinner();
	private final JComboBox cbPriorytet = new JComboBox();
	private final JComboBox cbDrukarnia = new JComboBox();
	private ArrayList<Autor> autorzy;
	private int idAutora, idTekstu, idDrukarni;
	private boolean zaakceptowano, czyWymuszonoDrukarnie; 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			OknoNoweZamowienie dialog = new OknoNoweZamowienie();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public int getIdAutora()
	{
		return idAutora;
	}
	
	public int getIdTekstu()
	{
		return idTekstu;
	}
	
	public int getIdDrukarni()
	{
		return idDrukarni;
	}
	
	public boolean czyZaakceptowano()
	{
		return zaakceptowano;
	}
	
	public int getNaklad()
	{
		return (int)spinnerNaklad.getValue();
	}
	
	public Priorytet getPriorytet()
	{
		return Priorytet.values()[cbPriorytet.getSelectedIndex()];
	}
	
	public void wypelnijAutorow(ArrayList<Autor>autorzy)
	{
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		model.addElement("<Wybierz>");
		this.autorzy = autorzy;
		for(Autor a:autorzy)
		{
			String daneAutora[] = a.toString().split("\t");
			model.addElement(daneAutora[0]+" "+daneAutora[1]);
		}
		cbAutorzy.setModel(model);
	}
	
	public void wypelnijDrukarnie(ArrayList<Drukarnia>drukarnie)
	{
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		model.addElement("<Wybierz>");
		for(Drukarnia d:drukarnie)
		{
			String daneDrukarni[] = d.toString().split("\t");
			model.addElement(daneDrukarni[1]);
		}
		cbDrukarnia.setModel(model);
	}
	
	private void wypelnijTeksty(int idAutora)
	{
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		model.addElement("<Wybierz>");
		for(TekstKultury t:autorzy.get(idAutora).getTeksty())
		{
			//JOptionPane.showMessageDialog(null, t.toString());
			String daneTekstu[] = t.toString().split("\t");
			model.addElement(new String("["+daneTekstu[0].charAt(0)+"] "+daneTekstu[3]));
		}
		cbTeksty.setModel(model);
	}
	
	/**
	 * Create the dialog.
	 */
	public OknoNoweZamowienie() {
		setTitle("Nowe zam\u00F3wienie");
		setBounds(100, 100, 219, 248);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblAutor = new JLabel("Autor");
			lblAutor.setBounds(12, 13, 56, 16);
			contentPanel.add(lblAutor);
		}
		{
			cbAutorzy.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent arg0) {
					if(cbAutorzy.getSelectedIndex() != 0)
					{
						wypelnijTeksty(cbAutorzy.getSelectedIndex()-1);
						idAutora = cbAutorzy.getSelectedIndex()-1;
					}
				}
			});
			cbAutorzy.setBounds(67, 10, 122, 22);
			contentPanel.add(cbAutorzy);
		}
		{
			JLabel lblTekst = new JLabel("Tekst");
			lblTekst.setBounds(12, 42, 48, 16);
			contentPanel.add(lblTekst);
		}
		{
			cbTeksty.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					idTekstu = cbTeksty.getSelectedIndex()-1;
				}
			});
			
			//cbTeksty.setse
			cbTeksty.setBounds(67, 42, 122, 22);
			contentPanel.add(cbTeksty);
		}
		{
			JLabel lblPriorytet = new JLabel("Priorytet");
			lblPriorytet.setBounds(12, 132, 56, 16);
			contentPanel.add(lblPriorytet);
		}
		{
			
			cbPriorytet.setModel(new DefaultComboBoxModel(new String[] {"Wysoki", "\u015Aredni", "Niski"}));
			cbPriorytet.setBounds(67, 129, 122, 22);
			contentPanel.add(cbPriorytet);
		}
		
		JLabel lblNakad = new JLabel("Nak\u0142ad");
		lblNakad.setBounds(12, 71, 56, 16);
		contentPanel.add(lblNakad);
		spinnerNaklad.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		
		
		spinnerNaklad.setBounds(67, 68, 122, 22);
		contentPanel.add(spinnerNaklad);
		{
			JLabel lblDrukarnia = new JLabel("Drukarnia");
			lblDrukarnia.setBounds(12, 100, 56, 16);
			contentPanel.add(lblDrukarnia);
		}
		cbDrukarnia.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				idDrukarni = cbDrukarnia.getSelectedIndex()-1;
			}
		});
		
		
		cbDrukarnia.setBounds(67, 97, 122, 22);
		contentPanel.add(cbDrukarnia);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						zaakceptowano = true;
						czyWymuszonoDrukarnie = autorzy.get(idAutora).getTeksty().get(idTekstu).toString().split("\t")[1].toString().equals("Album") && idDrukarni != 2;
						if(czyWymuszonoDrukarnie)
						{
							idDrukarni = 2;
							JOptionPane.showMessageDialog(null, "Wybrany tekst kultury jest albumem, wi�c zosta� przydzielony do odpowiedniej drukarni.");
						}
						if(cbAutorzy.getSelectedIndex() != 0 && cbDrukarnia.getSelectedIndex() != 0 && cbTeksty.getSelectedIndex() != 0)
							dispose();
						else
						{
							JOptionPane.showMessageDialog(null, "Wype�nij wymagane pola!");
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						zaakceptowano = false;
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
