package Wydawnictwo;

import java.util.ArrayList;
import java.util.Date;

/**
 * Klasa reprezentuj�ca Dzia� Programowy. 
 * @author Daniel Chrzanowski
 *
 */
public class DzialProgramowy implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Lista autor�w, kt�rzy wsp�pracuj� z wydawnictwem.
	 * @see Autor
	 */
	ArrayList<Autor> autorzy;
	
	/**
	 * Lista um�w, kt�re zawar�o wydawnictwo z autorami.
	 * @see Umowa
	 */
	ArrayList<Umowa> umowy;
	
	/**
	 * Konstruktor inicjalizuj�cy obiekt.
	 */
	public DzialProgramowy() {
		this.autorzy = new ArrayList<Autor>();
		this.umowy = new ArrayList<Umowa>();
	}
	
	/**
	 * Zawiera umow� o prace lub o dzie�o, z danym autorem.
	 * @param imieAutora - imie autora, z kt�rym wydawnictwo zawiera umow�.
	 * @param nazwiskoAutora - nazwisko autora, z kt�rym wydawnictwo zawiera umow�.
	 * @param czasRozpoczecia - czas rozpocz�cia umowy.
	 * @param czasZakonczenia - czas zako�czenia umowy.
	 * @param czyODzielo - Sprawdza czy umowa jest umow� o dzie�o.
	 * @param opis -  opis zamawianego dzie�a.
	 */
	public void zawrzyjUmowe(String imieAutora, String nazwiskoAutora, Date czasRozpoczecia, Date czasZakonczenia,
			boolean czyODzielo, String opis) {
		if (!czyODzielo)
			umowy.add(new UmowaOPrace(czasRozpoczecia, czasZakonczenia, getAutor(imieAutora, nazwiskoAutora)));
		else
			umowy.add(new UmowaODzielo(czasRozpoczecia, czasZakonczenia, getAutor(imieAutora, nazwiskoAutora), opis));
	}
	
	/**
	 * Rozwi�zuje umow� o prace lub o dzie�o, z danym autorem.
	 * @param x indeks umowy
	 */
	public void rozwiazUmowe(int x) {
		umowy.remove(x);
	}
	
	/**
	 * Zwraca list� um�w.
	 * @return Lista wszystkich um�w.
	 */
	public ArrayList<Umowa> przegladUmow() {
		return umowy;
	}

	/**
	 * Zwraca szukanego autora.
	 * @return autor.
	 * @param imieAutora - imie  szukanego autora.
	 * @param nazwiskoAutora - nazwisko szukanego autora.
	 */
	public Autor getAutor(String imieAutora, String nazwiskoAutora) {

		for (Autor d : autorzy) {
			if (d.imie.equals(imieAutora) && d.nazwisko.equals(nazwiskoAutora))
				return d;
		}
		return null;
	}

	/**
	 * Dodaje autora do listy autor�w
	 * @param autor - wskaznik na danego autora.
	 */
	public void dodajAutora(Autor autor) {

		if (getAutor(autor.imie, autor.nazwisko) == null)
			autorzy.add(autor);
	}
	
	 /**
	  * Aktualizuje umowy wzgl�dem czasu
	  * @param czas data wzgl�dem kt�rej ma si� zaktualizowa� status umowy
	  */
	public void aktualizujStan(Date czas) {
		for (Umowa d : umowy) {
			d.aktualizujStatus(czas);
		}
	}
	
	/**
	 * Usuwa danego autora z listy autor�w.
	 * @param imieAutora - imie usuwanego autora.
	 * @param nazwiskoAutora - nazwisko usuwanego autora.
	 */
	public void usunAutora(String imieAutora, String nazwiskoAutora) {
		autorzy.remove(getAutor(imieAutora, nazwiskoAutora));
	}
	
	/**
	 * Zwraca list� autor�w.
	 * @return Lista wszystkich autor�w.
	 */
	public ArrayList<Autor> getAutorzy() {
		return autorzy;
	}
	
	public void usunWygasleUmowy(){
	
		for (int x=umowy.size()-1;x>=0;x--) {
			if(umowy.get(x).czyWygasla())
			umowy.remove(x);
		}
	}

}
