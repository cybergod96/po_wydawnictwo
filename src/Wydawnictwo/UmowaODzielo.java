package Wydawnictwo;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Klasa reprezentuj�ca umow� o dzie�o.
 * @author Daniel Chrzanowski
 *
 */
public class UmowaODzielo extends Umowa implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * opis dzie�a.
	 *  
	 */
	String opis;
	
	/**
	 * Konstruktor inicjalizuj�cy obiekt.
	 * @param czasRozpoczecia -czas rozpoczecia danej umowy.
	 * @param czasZakonczenia - czas zako�czenia danej umowy.
	 * @param autor - wskaznik na danego autora.
	 * @param opis - opis dzie�a.
	 */
	 public UmowaODzielo(Date czasRozpoczecia, Date czasZakonczenia, Autor autor, String opis) {
		 
		super(czasRozpoczecia, czasZakonczenia, autor); this.opis=opis;
	}
	
	 public String toString(){
		 SimpleDateFormat formatDaty = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		 
		 if(czyWygasla())
		 return "Umowa o dzie�o"+"\t"+formatDaty.format(czasRozpoczecia)+"\t"+formatDaty.format(czasZakonczenia)+"\t"+opis+"\t"+"Wygas�a";
		 else
			 return "Umowa o dzie�o"+"\t"+formatDaty.format(czasRozpoczecia)+"\t"+formatDaty.format(czasZakonczenia)+"\t"+opis+"\t"+"Trwa";

	 }
}
