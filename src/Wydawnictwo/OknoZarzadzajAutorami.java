package Wydawnictwo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class OknoZarzadzajAutorami extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ArrayList<Autor> autorzy;
	private JTable tableListaAutorow;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			OknoZarzadzajAutorami dialog = new OknoZarzadzajAutorami();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void aktualizujTabeleAutorow()
	{
		DefaultTableModel model = (DefaultTableModel) tableListaAutorow.getModel();
		for(int i = model.getRowCount()-1; i >= 0; i-- )
		{
			model.removeRow(i);
		}
		
		
			for(Autor a: autorzy)
			{
				String daneAutora[] = a.toString().split("\t");
				model.addRow(new Object[]{
						daneAutora[0],daneAutora[1],daneAutora[2]});
				
			}
		
	}
	
	public void wypelnijAutorow(ArrayList<Autor>autorzy)
	{
		DefaultTableModel model = (DefaultTableModel) tableListaAutorow.getModel();
		this.autorzy = autorzy;
		for(Autor a:autorzy)
		{	
		
			String daneAutora[] = a.toString().split("\t");
				model.addRow(new Object[]{
						daneAutora[0],daneAutora[1],daneAutora[2]});
		
		}
			tableListaAutorow.setModel(model);
	}
	
	
	private void removeSelectedRows() {
		int indeks = tableListaAutorow.getSelectedRow();
		DefaultTableModel model = (DefaultTableModel) tableListaAutorow.getModel();
		model.removeRow(indeks);
		tableListaAutorow.setModel(model);
	autorzy.remove(indeks);
	}

	/**
	 * Create the dialog.
	 */
	public OknoZarzadzajAutorami() {
		setTitle("Zarz\u0105dzanie autorami");
		setBounds(100, 100, 590, 376);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(12, 13, 421, 260);
			contentPanel.add(scrollPane);
			{
				tableListaAutorow = new JTable();
				tableListaAutorow.setModel(new DefaultTableModel(
					new Object[][] {
					},
					new String[] {
							"Imi�", "Nazwisko", "Napisane teksty"
					}
				));
				scrollPane.setViewportView(tableListaAutorow);
			}
		}
		{
			JButton btnDodaj = new JButton("Dodaj");
			btnDodaj.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					
							OknoDodajAutora okno = new OknoDodajAutora();
							okno.pobierzListe(autorzy);
							
							okno.setModal(true);
							okno.setVisible(true);
							if(okno.czyZaakceptowano())
							{	boolean istnieje=false;
							
								for (Autor d : autorzy) 
									if (d.imie.equals(okno.getImie()) && d.nazwisko.equals(okno.getNazwisko()))
										{JOptionPane.showMessageDialog(null, "Dany autor ju� istnieje");
									istnieje=true;}
									
								if(!istnieje)
										autorzy.add(new Autor(okno.getImie(),okno.getNazwisko()));
	
								aktualizujTabeleAutorow();
									}
				}
			});
			btnDodaj.setBounds(445, 17, 97, 25);
			contentPanel.add(btnDodaj);
		}
		{
			JButton btnUsun = new JButton("Usu\u0144");
			btnUsun.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {					
		                  removeSelectedRows();		            		               
				}
			});
			btnUsun.setBounds(445, 55, 97, 25);
			contentPanel.add(btnUsun);
		}
		{
			JButton btnNapiszTekst = new JButton("Napisz tekst");
			btnNapiszTekst.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if(tableListaAutorow.getSelectedRow()==-1)
						JOptionPane.showMessageDialog(null, "Zaznacz autora");
					else
					{
					OknoNapiszTekst okno = new OknoNapiszTekst();
					
					
					okno.setModal(true);
					okno.setVisible(true);
					if(okno.czyZaakceptowano())
					{
						if(okno.getRodzaj()==1)
						autorzy.get(tableListaAutorow.getSelectedRow()).napiszKsiazke(okno.getTytul(), okno.getGatunek());
						if(okno.getRodzaj()==2||okno.getRodzaj()==3)
							autorzy.get(tableListaAutorow.getSelectedRow()).napiszCzasopismo(okno.getTytul(), okno.czyTygodnik());
											
						aktualizujTabeleAutorow();
							}
				}}
			});
			btnNapiszTekst.setBounds(445, 93, 117, 25);
			contentPanel.add(btnNapiszTekst);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
