package Wydawnictwo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class OknoNapiszTekst extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private final JComboBox cbGatunek = new JComboBox();
	private final JComboBox cbRodzaj = new JComboBox();
	private final JLabel lblGatunek = new JLabel("Gatunek");
	private boolean zaakceptowano;
	private	String	getTytul,getGatunek,getRodzaj;
	private int idRodzaj;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			OknoNapiszTekst dialog = new OknoNapiszTekst();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getTytul()
	{
		return textField.getText();
	}

	public int getRodzaj()
	{
		return cbRodzaj.getSelectedIndex()+1;
	}
	public boolean czyTygodnik()
	{
		if(cbRodzaj.getSelectedIndex()==1)
		return true;
		else
			return false;
	}
	public GatunekKsiazki getGatunek()
	{
		return GatunekKsiazki.values()[cbGatunek.getSelectedIndex()];
	}
	public boolean czyZaakceptowano()
	{
		return zaakceptowano;
	}	
	/**
	 * Create the dialog.
	 */
	public OknoNapiszTekst() {
		setTitle("Napisz tekst");
		setBounds(100, 100, 274, 175);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblTytu = new JLabel("Tytu\u0142");
			lblTytu.setBounds(12, 13, 56, 16);
			contentPanel.add(lblTytu);
		}
		{
			textField = new JTextField();
			textField.setBounds(80, 10, 151, 22);
			contentPanel.add(textField);
			textField.setColumns(10);
		}
		{
			JLabel lblRodzaj = new JLabel("Rodzaj");
			lblRodzaj.setBounds(12, 46, 56, 16);
			contentPanel.add(lblRodzaj);
			idRodzaj = cbRodzaj.getSelectedIndex();
		}		
		{			
			cbRodzaj.setModel(new DefaultComboBoxModel(new String[] {"Ksiazka", "Tygodnik", "Miesiecznik"}));
			cbRodzaj.setBounds(80, 43, 151, 22);
			contentPanel.add(cbRodzaj);
		}
		

		{
			
			lblGatunek.setBounds(12, 79, 56, 16);
			contentPanel.add(lblGatunek);
			//contentPanel.remove(lblGatunek);
		}
		{					
			cbGatunek.setModel(new DefaultComboBoxModel(new String[] {"Sensacyjna", "Romans", "Album","SciFi", "Podr�cznik", "Naukowa"}));
			cbGatunek.setBounds(80, 76, 151, 22);
			contentPanel.add(cbGatunek);
		}
		
		cbRodzaj.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(cbRodzaj.getSelectedIndex()!=0)
				{
					contentPanel.remove(cbGatunek);
				}
				if(cbRodzaj.getSelectedIndex()==0)
				{					
					contentPanel.add(cbGatunek);
				}
			}
		});
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						zaakceptowano = true;
						if(!textField.getText().isEmpty())
								dispose();
							else
							JOptionPane.showMessageDialog(null, "Wype�nij wymagane pola!");
						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						zaakceptowano = false;
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
