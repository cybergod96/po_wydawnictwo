package Wydawnictwo;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class TestDodawaniaZamowienia {

	@Test
	public void test() {
		DzialDruku d = new DzialDruku();
		d.zlozZamowienie(new Ksiazka("Testowa ksi��ka",123,GatunekKsiazki.Sensacyjna), 1000, Priorytet.Sredni, new Date(), 0);
		assertEquals(1, d.przegladZamowien(0).size());
	}

}
