package Wydawnictwo;

/**
 * Klasa reprezentuj�ca drukarni�, kt�ra specjalizuje si� w wydruku album�w.
 * @author Piotr Cywoniuk
 * @see Drukarnia
 */
public class DrukarniaAlbumow extends Drukarnia implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	public DrukarniaAlbumow(String nazwa, double szybkoscDrukuStrony, int iloscDrukarek) {
		super(nazwa, szybkoscDrukuStrony, iloscDrukarek);
	}
	
	public String toString()
	{
		return new String("Albumy\t"+nazwa+"\t"+szybkoscDrukuStrony+"\t"+iloscDrukarek);
	}
	
}
