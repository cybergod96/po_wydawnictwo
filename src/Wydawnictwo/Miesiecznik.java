package Wydawnictwo;

/**
 * Klasa reprezentująca Miesiecznik.
 * @author Daniel Chrzanowski
 *
 */
public class Miesiecznik extends Czasopismo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Konstruktor inicjalizujący obiekt.
	 * @param tytul tytuł danego miesiecznika.
	 * @param iloscStron - ilość stron danego miesiecznika.
	 */
	public Miesiecznik(String tytul, int iloscStron) {
		super(tytul, iloscStron);

	}

	public String toString() {
		if (dataWydania != null)
			return "Czasopismo" + "\t" + "Miesiecznik" + "\t" + issn + "\t" + tytul + "\t" + iloscStron + "\t"
					+ dataWydania;
		else
			return "Czasopismo" + "\t" + "Miesiecznik" + "\t" + issn + "\t" + tytul + "\t" + iloscStron + "\t"
					+ "Nie wydano";
	}
}