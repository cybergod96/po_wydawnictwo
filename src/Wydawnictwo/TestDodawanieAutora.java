package Wydawnictwo;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class TestDodawanieAutora {

	@Test
	public void test() {
		DzialProgramowy d = new DzialProgramowy();
		d.dodajAutora(new Autor("Adam","Lwowicz"));
		assertEquals(1,d.getAutorzy().size());
	}

}
