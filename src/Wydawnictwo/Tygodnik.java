package Wydawnictwo;


/**
 * Klasa reprezentująca Tygodnik.
 * @author Daniel Chrzanowski
 *
 */
public class Tygodnik extends Czasopismo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Konstruktor inicjalizujący obiekt.
	 * @param tytul - tytuł danego tygodnika.
	 * @param iloscStron - ilość stron danego tygodnika.
	 */
	public Tygodnik(String tytul, int iloscStron) {
		super(tytul, iloscStron);
	}

	public String toString() {
		if (dataWydania != null)
			return "Czasopismo" + "\t" + "Tygodnik" + "\t" + issn + "\t" + tytul + "\t" + iloscStron + "\t"
					+ dataWydania;
		else
			return "Czasopismo" + "\t" + "Tygodnik" + "\t" + issn + "\t" + tytul + "\t" + iloscStron + "\t"
					+ "Nie wydano";
	}
}