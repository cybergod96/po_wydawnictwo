package Wydawnictwo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Klasa reprezentująca umowę o prace.
 * @author Daniel Chrzanowski
 *
 */
public  class UmowaOPrace extends Umowa implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * Konstruktor inicjalizujący obiekt.
	 * @param czasRozpoczecia -czas rozpoczecia danej umowy.
	 * @param czasZakonczenia - czas zakończenia danej umowy.
	 * @param autor - wskaznik na danego autora.
	 */
	public UmowaOPrace(Date czasRozpoczecia, Date czasZakonczenia, Autor autor) {
		 
		super(czasRozpoczecia, czasZakonczenia, autor);

	}

	public String toString(){
		 SimpleDateFormat formatDaty = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		if(czyWygasla())
		return "Umowa o prace"+"\t"+formatDaty.format(czasRozpoczecia)+"\t"+formatDaty.format(czasZakonczenia)+"\t \t"+"Wygasło";
		else
			return "Umowa o prace"+"\t"+formatDaty.format(czasRozpoczecia)+"\t"+formatDaty.format(czasZakonczenia)+"\t \t"+"Trwa";
	 }
 }