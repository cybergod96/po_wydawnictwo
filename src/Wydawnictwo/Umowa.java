package Wydawnictwo;

import java.util.Date;

/**
 * Klasa abstrakcyjna reprezentująca umowę.
 * @author Daniel Chrzanowski
 *
 */
public  abstract class Umowa implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;
	
	/**
	* Data rozpoczęcia i zakończenia umowy.
	*/
	Date czasRozpoczecia,czasZakonczenia;
	
	/**
	 * Obiekt reprezentujący autora.
	 * 
	 * @see Autor
	 */
	 Autor autor;

	 boolean wygasla;
	 
		/**
		 * Konstruktor inicjalizujący obiekt.
		 * @param czasRozpoczecia -czas rozpoczecia danej umowy.
		 * @param czasZakonczenia - czas zakończenia danej umowy.
		 * @param autor - autor, z którym jest zawierana umowa.
		 */
	 public Umowa(Date czasRozpoczecia,Date czasZakonczenia, Autor autor){
		 this.autor=autor; this.czasRozpoczecia=czasRozpoczecia; this.czasZakonczenia=czasZakonczenia;this.wygasla=false;
	 }
	 /**
	  * 
	  * @param czas data względem której ma się zaktualizować status umowy
	  */
	 public void aktualizujStatus(Date czas){
		 wygasla = czas.after(czasZakonczenia);
		 }
	 
		/**
		* 
		* @return Zwraca wskaznik na autora
		*/
	 public Autor getAutor(){
		 return autor;
	 }
	 public String toString(){
		 return null;
	 }
	 /**
	  * Sprawdza status umowy
	  * @return zwraca true jeśli umowa wygasła
	  */
	 public boolean czyWygasla()
	 {
		 return wygasla;
	 }
}
