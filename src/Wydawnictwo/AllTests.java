package Wydawnictwo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestDodawaniaZamowienia.class, TestDodawanieAutora.class, TestUsuwaniaRealizowaniaZamowien.class,
		TestUsuwaniaUmowy.class })
public class AllTests {

}
