package Wydawnictwo;

/**
 * Klasa reprezentuj�ca drukarni� drukuj�c� czasopisma i ksi��ki nieb�d�ce albumami.
 * @author Piotr Cywoniuk
 * @see Drukarnia
 */
public class DrukarniaZwyklych extends Drukarnia implements java.io.Serializable {

	private static final long serialVersionUID = 1L;


	public DrukarniaZwyklych(String nazwa, double szybkoscDrukuStrony, int iloscDrukarek) {
		super(nazwa, szybkoscDrukuStrony, iloscDrukarek);
	}
	
	
	public String toString()
	{
		return new String("Ksi��ki/Czasopisma\t"+nazwa+"\t"+szybkoscDrukuStrony+"\t"+iloscDrukarek);
	}
}
